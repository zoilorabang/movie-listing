import React from 'react';
import {Header} from './features/header';
import {Movies} from './features/movies';

function App() {
  return (
    <div className="App">
      <Header />
      <Movies />
    </div>
  );
}

export default App;
