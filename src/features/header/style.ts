
import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {
        display: 'flex',
        padding: '15px 30px'
    },
    logo: {
        width: 140
    },
});