import React, { useState } from 'react';
import { useStyles } from "./style";

export const Header = () => {
    const classes = useStyles();
    return (<>
        <header className={classes.root}>
            <img src="./zcr-logo.png" className={classes.logo}/>
        </header>
    </>)
}