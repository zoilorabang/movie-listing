import React, { useState } from 'react';
import { useAppDispatch,useAppSelector } from '../../app/hooks';
import { useStyles } from "./style";
import {
    FormControl,
    InputLabel,
    Select,
    MenuItem
} from '@material-ui/core';
import {
    filterByEvent,
    selectMovies
} from './movieSlice';

const filterLabel: any = {
    all: "All",
    productionYear: "Year",
    genre: "Genre"
}

export const MovieFilter = () => {
    const classes = useStyles();
    const dispatch = useAppDispatch();
    const movies = useAppSelector(selectMovies);
    const [filterBy, setFilterBy] = useState('0');
    const [filterValue, setFilterValue] = useState('');

    const handleChange = (event: React.ChangeEvent<{ value: unknown }>) => {
        setFilterBy(event.target.value as string);
        setFilterValue('');
        dispatch(
            filterByEvent({
               filter: event.target.value as string,
               selectedFilterVal: ''
            })
        )
    };

    const handleFilterValChange = (event: React.ChangeEvent<{ value: unknown }>) =>{
        setFilterValue(event.target.value as string);
        dispatch(
            filterByEvent({
               filter: filterBy as string,
               selectedFilterVal: event.target.value as string
            })
        )
    }

    return (<div>
        <FormControl className={classes.formControl}>
            <InputLabel>Filter By</InputLabel>
            <Select
                value={filterBy}
                onChange={handleChange}
                >
                <MenuItem value="0">{filterLabel.all}</MenuItem>
                <MenuItem value={"productionYear"}>{filterLabel.productionYear}</MenuItem>
                <MenuItem value={"genre"}>{filterLabel.genre}</MenuItem>
            </Select>
        </FormControl>

        {filterBy !== "0" &&
        <FormControl className={classes.formControl}>
            <InputLabel>Select {filterLabel[filterBy]}</InputLabel>
            <Select
                value={filterValue}
                onChange={handleFilterValChange}
                >
                {movies.filterDropDown[filterBy] && movies.filterDropDown[filterBy].map((row: any, key: number)=>{
                    return (<MenuItem value={row} key={key}>{row}</MenuItem> )
                })}
            </Select>
        </FormControl>}
    </div>)
}