import { createAsyncThunk, createSlice, PayloadAction, createEntityAdapter, Action } from '@reduxjs/toolkit';
import { RootState, AppThunk } from '../../app/store';
import { fetchMovieList} from './movieAPI';

interface Dropdown {
  productionYear: Array<string>;
  genre: Array<string>;
}

export interface MovieState {
    list: [];
    filteredList: [];
    filter: '0' | 'productionYear' | 'genre';
    selectedFilterVal: any;
    status: 'idle' | 'loading' | 'failed';
    filterDropDown: any;
}
  
const initialState: MovieState = {
    list: [],
    filteredList: [],
    filter: '0',
    selectedFilterVal: '',
    status: 'idle',
    filterDropDown: {}
};


export const movieListAsync = createAsyncThunk(
    'movie/fetchMovieList',
    async () => {
      const response = await fetchMovieList();
      return response;
    }
);


export const movieSlice = createSlice({
    name: 'movies',
    initialState,
    reducers: {
      listDownMovies: (state, action: PayloadAction<any>) =>{
        state.list = action.payload;
      },
      filterByGenre: (state, action:PayloadAction<any>) => {
        state.filteredList = action.payload
      }
    },
    extraReducers: (builder) => {
      builder
        .addCase(movieListAsync.pending, (state) => {
          state.status = 'loading';
        })
        .addCase(movieListAsync.fulfilled, (state, action) => {

          let year: Array<string> = [], genre: Array<string>= [];
          if(action.payload && action.payload.length > 0){
            action.payload.map((row: any)=>{
                if(row.productionYear && !year.includes(row.productionYear.toString())){
                  year.push(row.productionYear.toString());
                }
                if(row.genre && !genre.includes(row.genre.toString())){
                  genre.push(row.genre.toString());
                }
            });
          }

          let newDropdown: Dropdown = {
            productionYear: year.sort((a,b) =>  (a > b ? 1 : -1)),
            genre: genre.sort((a,b) =>  (a > b ? 1 : -1))
          }

          state.filterDropDown = newDropdown;
          state.status = 'idle';
          state.list   = action.payload;
        });
    },
});

export const { listDownMovies, filterByGenre } = movieSlice.actions;

export const selectMovies = (state: RootState) => state.movies;

export const filterByEvent = (data: any): AppThunk => (
    dispatch,
    getState
  ) => {
    const currentValue = listDownMovies(getState());
    
    let newFilteredVal: Array<Object> = [];
    if (currentValue.payload.movies.list.length > 0 && data.filter !== '0') {
      currentValue.payload.movies.list.map((row: any)=> {
        if(row[data.filter] && row[data.filter].toString() === data.selectedFilterVal){
          newFilteredVal.push(row);
        }
      });
    }
   
    dispatch(filterByGenre(newFilteredVal));
};

export default movieSlice.reducer;