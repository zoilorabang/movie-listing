const MOVIELIST = 'https://sometimes-maybe-flaky-api.gdshive.io/';

export function fetchMovieList() {
    return fetch(MOVIELIST)
		.then((response) => response.json());
}
  