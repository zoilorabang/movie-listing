
import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
    root: {
        padding: '15px 30px'
    },
    thumbnails: {
        textAlign: 'center',
        "& .MuiGrid-item":{
            padding: '4px'
        },
        '& img':{
            width: '100%',
            cursor: 'pointer',
            "&:hover":{
                opacity: 0.8
            }
        }
    },
    formControl: {
        margin: 15,
        minWidth: 120,
      },
    selectEmpty: {
        marginTop: 15,
    },
});