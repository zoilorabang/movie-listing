import React, { useEffect, useState } from 'react';
import { useAppSelector, useAppDispatch } from '../../app/hooks';
import {Loader} from '../loader';
import {CustomDrawer} from '../drawer';
import {
    Grid
} from '@material-ui/core';
import { useStyles } from "./style";
import {MovieFilter} from "./movieFilter";
import {
    movieListAsync,
    selectMovies
  } from './movieSlice';

interface MovieDetails {
    name: string;
    synopsisShort: string;
    synopsis: string;
    productionYear: number,
    genre: string;
    image: string;
} 

export const Movies = () => {
    const classes = useStyles();
    const movies = useAppSelector(selectMovies);
    const dispatch = useAppDispatch();
    const [details, setDetails] = useState<MovieDetails | undefined>(undefined);
    
    useEffect(()=>{
        dispatch(movieListAsync());
    },[]);

    const handleOnClick = (e: any) => {
        const movieD = JSON.parse(e.currentTarget.id);
        setDetails(movieD);
    }

    const onMovieDetailsClose = (stat: boolean) => {
        if(!stat) setDetails(undefined);
    }

    const movielist = movies.filteredList.length > 0 ? movies.filteredList : movies.list;
    return (<>
        <div className={classes.root}>
            {(movies.status !== 'idle') && <Loader />}
            <MovieFilter />
            <br />
            <Grid container className={classes.thumbnails}>
                {(movielist && movielist.length > 0) && movielist.map((movie: any,key)=>{
                    return (
                    <Grid item xs={6} sm={4} md={2} key={key}>
                        <span onClick={handleOnClick} key={'s'+key} id={JSON.stringify(movie)}>
                            <img src={`./images/${movie.image}`}/>
                        </span>
                    </Grid>
                    )
                })}
            </Grid>
            {(details && Object.keys(details).length > 0) && <CustomDrawer 
                open={true} 
                title={details.name}
                content={generateMovieDetails(details)}
                onPopUpClose={onMovieDetailsClose}
                />}
        </div>
    </>);
}


const generateMovieDetails = (data: any) => {

    return (<>  
         <Grid container>
            <Grid item sm={12} md={3}><img src={`./images/${data.image}`} /></Grid>
            <Grid item sm={12} md={9}>
                <h2>{data.name}</h2>
                <h3>Genre: <small>{data.genre}</small></h3>
                <h3>Production Year: <small>{data.productionYear}</small></h3>
                <p dangerouslySetInnerHTML={{__html: data.synopsisShort}} className={'shortdesc'}/>
                <p dangerouslySetInnerHTML={{__html:data.synopsis}} className={'fulldesc'}/>
            </Grid>
         </Grid>   
    </>)
}