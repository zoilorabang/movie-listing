import { render } from '@testing-library/react';
import movieReducer, {
    MovieState,
    filterByGenre
} from './movieSlice';
import {MovieFilter} from "./movieFilter";

const initialState: MovieState = {
    list: [],
    filteredList: [],
    filter: '0',
    selectedFilterVal: '',
    status: 'idle',
    filterDropDown: {}
};

const sampleList = [
    {
        name:'Testing name',
        productionYear:'2021',
        genre: 'Work',
        synopsisShort: 'Test short content',
        synopsis: 'Test description',
        image:'testimage.jpg'
    }
]

describe('Movies reducer', () => {
    it('should handle initial state', () => {
      expect(movieReducer(undefined, { type: 'unknown' })).toEqual({
        list: [],
        filteredList: [],
        filter: '0',
        selectedFilterVal: '',
        status: 'idle',
        filterDropDown: {}
      });
    });
  
    it('should handle dropdown filter initial state', () => {
        expect(movieReducer(undefined, { type: 'unknown' })).toEqual({
            list: [],
            filteredList: [],
            filter: '0',
            selectedFilterVal: '',
            status: 'idle',
            filterDropDown: {}
        });
    });
});

describe('<MovieFilter />', () => {

    it('should run dropdown without crashing', () => {

    });

    it('should handle increment', () => {
        const actual = movieReducer(initialState, filterByGenre(sampleList));
        expect(actual.filteredList).toEqual(sampleList);
    });
});
