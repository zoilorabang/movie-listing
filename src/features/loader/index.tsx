import React from 'react';
import { useStyles } from "./style";
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

export const Loader = () => {
    const classes = useStyles();
    return (<>
        <Backdrop className={classes.backdrop} open={true}>
            <CircularProgress color="inherit" />
        </Backdrop>
    </>)
}