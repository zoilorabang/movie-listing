import React, {useEffect} from 'react';
import {
    Button,
    Dialog,
    AppBar,
    Toolbar,
    IconButton,
    Typography,
    Slide
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';
import { TransitionProps } from '@material-ui/core/transitions';
import { useStyles } from "./style";


const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export const CustomDrawer = (props: any) => {
  const classes = useStyles();
  const [open, setOpen] = React.useState(props.open);

  const handleClose = () => {
    setOpen(false);
    if(props.onPopUpClose) props.onPopUpClose(false)
  };

  useEffect(()=>{
    setOpen(props.open)
  },[props.open]);


  return (
    <div>
      <Dialog fullScreen open={open} onClose={handleClose} TransitionComponent={Transition}>
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
              {props.title}
            </Typography>
          </Toolbar>
        </AppBar>
        <div className={classes.detailsSection}>
            {props.content && props.content}
        </div>
      </Dialog>
    </div>
  );
}