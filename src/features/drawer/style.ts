
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      position: 'relative',
      backgroundColor: '#26a9e0'
    },
    title: {
      marginLeft: theme.spacing(2),
      flex: 1,
    },
    detailsSection:{
      margin: '30px 25px',
      "& img":{
          width: '95%'
      }
    }
  }),
);