### Exam source code:
React *movie listing with filtering* in a small webapp.

#### Installation:
* Clone the repository from "git clone https://rabangjay@bitbucket.org/zoilorabang/movie-listing.git"
* Install npm module "npm install"

#### Run the application:
* run "npm run start"

#### Test the small webapp:
* run "npm run test"

#### Requirements:
* NodeJS
* Git
* Any Editor (Visual Studio Code, notepad++)
